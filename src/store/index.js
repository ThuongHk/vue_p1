import Vue from 'vue';
import VueX from 'vuex'

import author from './modules/author';
import todos from './modules/todos'
Vue.use(VueX)

const storeData = {
   modules: {
    author,
    todos
   }

}

const store = new VueX.Store(storeData)
export default store