const  state = {
    isAuthor : true

}

const getters = {
    isAuthor: state => state.isAuthor,
}


const mutations = {
    TOGGLEAUTHOR(state){
        state.isAuthor = !state.isAuthor;         
       },
}

export default {
    state,    
    getters,
    mutations
}



