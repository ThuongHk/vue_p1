import axios from "axios"

const todoModules = {
    state: {
        todos: [
            {
                id: 1,
                title: 'viec 1',
                completed: true
            },
            {
                id: 2,
                title: 'viec 2',
                completed: false
            },
            {
                id: 3,
                title: 'viec 3',
                completed: false
            }
        ],
       
    },
    getters: {        
        
        todos: state => state.todos,
        doneTodo: state => state.todos.filter(todo => todo.completed).length,
        totalJob: state => state.todos.length
        

    },
    actions: {
        deleteTodoItem({commit}, todoId){
            
            commit('DELETETODO', todoId)
        },
       async addTodoItem({commit}, newTodo){
        try {
             await axios.post(`https://jsonplaceholder.typicode.com/todos`,newTodo)
            commit('ADDTODO', newTodo )
        } catch (error) {
            console.log(error)
        }
            
        },
       async getTodo({commit}){
           try {
            let res = await axios.get(`https://jsonplaceholder.typicode.com/todos?_limit=5`)
            console.log(res)
            commit('GETTODO',res.data)
           } catch (error) {
            console.log(error)
           }
        }
    },
    mutations: {
       
        DELETETODO(state, todoId) {
            state.todos = state.todos.filter(todo => todo.id !== todoId)
        },
        CHECKEDBOX(state, todoId){
            state.todos = state.todos.map(todo => { if(todo.id === todoId) todo.completed = !todo.completed
            return todo
            })
        },
        ADDTODO(state, newTodo){
            state.todos.push(newTodo)
        },
        GETTODO(state, arrTodo){
            state.todos = arrTodo
        }
    }
}

export default todoModules